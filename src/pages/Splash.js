import React, {Component} from 'react';
import { View,Text,StyleSheet,Image } from 'react-native';
import { StackActions } from '@react-navigation/native';
import Foto from './Image';

class Splash extends Component {

    componentDidMount(){
        setTimeout(() => {
            this.props.navigation.dispatch(StackActions.replace('Home'))
        }, 3000);
    }

    render(){
        return (
            <View style={style.mama}>
                <Foto />
            </View>
        );
    }
}

export default Splash;

const style = StyleSheet.create ({
    container: {

    },
    mama: {
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
    }
});