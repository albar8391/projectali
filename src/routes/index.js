import {View,Text, } from 'react-native';
import React from 'react'
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import {Home,Cari,PartTiga } from '../pages/index';
import { PartDua,Splash } from '../pages/index';
const Navigasi = createNativeStackNavigator ();

const Route = () => {
    return (
        <Navigasi.Navigator
        initialRouteName='Splash'
        screenOptions={{headerShown: false}}>
            <Navigasi.Screen name="Home" component={Home } />
            <Navigasi.Screen name="PartDua" component={PartDua } />
            <Navigasi.Screen name="PartTiga" component={PartTiga } />
            <Navigasi.Screen name="Cari" component={Cari } />
            <Navigasi.Screen name="Splash" component={Splash } />
        </Navigasi.Navigator>
        
    );
};

export default Route;